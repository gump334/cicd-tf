resource "aws_iam_user" "iam_users" {
  for_each = toset(var.aws_iam_user_names)
  name = each.key

  tags = {
    tag-key = "iam-users"
  }
}