#root
module "vpc" {
  source = "./modules/vpc"
}
module "ec2" {
  source = "./modules/web"
  sn = module.vpc.pb_sn
  sg = module.vpc.pb_sg
}
module "iam" {
  source = "./modules/iam"
}
#test #test