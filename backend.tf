terraform {
  backend "s3" {
    bucket = "tf-lab-tfstate"
    key    = "tfbackend/terraform.tfstate"
    region = "us-east-1"
    dynamodb_table = "terraform-state-locking"
  }
}
